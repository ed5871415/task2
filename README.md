# Task2



## Побудуйте власну послідовність дій для двох логічних блоків та обґрунтуйте свій вибір. Об'єднайте деякі кроки разом, котрі вважаєте треба виконувати як один процесс у випадку ci-cd. Як результат отримати послідовність для майбутнього вибудовування ci-cd процесів для кожного з блоків.

### Блок 1

- unit tests (quick)
- code linters
- publishing to environment
- build code
- integration tests (take a lot of time)
- docker image creating

### Блок 2

- unit tests (quick)
- code linters
- release new package version in registry of packages
- build code
- create git tag for current branch
- publish static content for preview (static content is creating when run specific command for generate it)

**Для блоку 1, запропонована мною послідовність дій буде наступною:**

- Запустити юніт-тести (unit tests) та перевірити код за допомогою лінтерів (code linters).
- Збудувати код (build code) та створити Docker-образ (docker image creating).
- Розгорнути збудований код (publishing to environment) та запустити інтеграційні тести (integration tests).

**Обгрунтування:**

Юніт-тести та лінтери є швидкими процесами, тому їх варто запускати одразу на початку циклу CI/CD, щоб виявити потенційні проблеми в коді якомога раніше.

Збудувати код та створити Docker-образ доцільно зробити наступним кроком, оскільки ці процеси можуть бути тривалими та залежати від результатів перших двох кроків.

Розгорнути збудований код та запустити інтеграційні тести наприкінці, оскільки інтеграційні тести можуть тривати досить довго та залежать від успішного розгортання коду.


**Для блоку 2, запропонована мною послідовність дій буде наступною:**

- Запустити юніт-тести (unit tests) та перевірити код за допомогою лінтерів (code linters).
- Збудувати код (build code) та опублікувати статичний контент для превью (publish static content for preview).
- Створити Git-тег для поточної гілки (create git tag for current branch) та випустити нову версію пакета в реєстрі пакетів (release new package version in registry of packages).

**Обгрунтування:**

Юніт-тести та лінтери є швидкими процесами, тому їх варто запускати одразу на початку циклу CI/CD, щоб виявити потенційні проблеми в коді якомога раніше.

Збудувати код та опублікувати статичний контент для превью доцільно зробити наступним кроком, оскільки ці процеси можуть бути тривалими та залежати від результатів перших двох кроків.

Створити Git-тег та випустити нову версію пакета наприкінці, оскільки ці дії свідчать про успішне проходження всіх попередніх кроків і готовність до випуску нової версії.

У обох блоках я об'єднав юніт-тести та лінтери в один крок, оскільки ці процеси є швидкими та можуть виконуватись паралельно. Також я об'єднав збудування коду та створення Docker-образу (для блоку 1), та збудування коду та публікацію статичного контенту (для блоку 2), оскільки ці процеси можуть бути тривалими та залежати від результатів перших двох кроків.




